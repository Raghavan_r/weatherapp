package com.app.weather.listener

import android.location.Location

/**
 * This interface used to invoke the view model whenever the location change in GpsTracker class
 * */
interface LocationUpdateListener {

    /**
     * This method invoked the view model whenever the location changes happen in GpsTracker class
     * @param location is updated location from the GpsTracker
     * */
    fun updatedLocation(location: Location)
}