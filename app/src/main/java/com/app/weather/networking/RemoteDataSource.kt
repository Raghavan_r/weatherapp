package com.app.weather.networking

import android.content.Context
import com.app.weather.BuildConfig
import com.app.weather.di.WEATHER_SCOPE_QUALIFIER
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val SERVER_URL = "https://api.darksky.net"
const val TIME_OUT = 60L


val remoteDataSourceModule = module {

    scope(WEATHER_SCOPE_QUALIFIER) {
        scoped {
            createOkHttpClient(get())
        }

        scoped {
            createWebService<WeatherDataSource>(get())
        }
    }
}

/**
 * This method used to create okHttp client
 * */
fun createOkHttpClient(context: Context): OkHttpClient {

    val clientBuilder =
        OkHttpClient.Builder()
            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)

    clientBuilder.addInterceptor(HttpInterceptor(context))


        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        clientBuilder.addInterceptor(loggingInterceptor)
    return clientBuilder.build()
}

/**
 * Method used to create Retrofit instance
 * @param [okHttpClient] used to bind with retrofit
 * @param [url] is contain the base url of the app
 * */
inline fun <reified T> createWebService(okHttpClient: OkHttpClient): T {
    val retrofit =
        Retrofit.Builder()
            .baseUrl(SERVER_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()

    return retrofit.create(T::class.java)
}

