package com.app.weather.networking

/**
 * An enum class which holds the key for network status
 * */
enum class Status {
        SUCCESS,
        ERROR,
        LOADING
}
