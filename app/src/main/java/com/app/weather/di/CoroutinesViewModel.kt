package com.app.weather.di

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

/**
 * ViewModel with coroutine scope that can be used by other ViewModels that use coroutines
 */
abstract class CoroutinesViewModel(parentJob: Job? = null) : ViewModel() {

    protected val viewModelScope: CoroutineScope = ViewModelCoroutineScope(parentJob)

    override fun onCleared() {
        viewModelScope.cancel()
        super.onCleared()
    }
}

private class ViewModelCoroutineScope(parentJob: Job?) : CoroutineScope {

    private val job = SupervisorJob(parentJob)
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main
}
