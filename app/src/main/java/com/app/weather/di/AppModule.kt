package com.app.weather.di

import com.app.weather.networking.ResponseHandler
import com.app.weather.networking.remoteDataSourceModule
import com.app.weather.repo.WeatherRepo
import com.app.weather.repoImpl.WeatherRepoImpl
import com.app.weather.usecase.WeatherHomeUseCase
import com.app.weather.viewModel.SplashViewModel
import com.app.weather.viewModel.WeatherHomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val WEATHER_SCOPE_ID = "WEATHER_HOME"

val WEATHER_SCOPE_QUALIFIER = named(WEATHER_SCOPE_ID)


val weatherAppModule = module {
    scope(WEATHER_SCOPE_QUALIFIER) {
        viewModel {
            SplashViewModel()
        }
        viewModel {
            WeatherHomeViewModel(get(),get())
        }
    }

}

val useCaseModule = module {
    scope(WEATHER_SCOPE_QUALIFIER){
        scoped {
            WeatherHomeUseCase(get(),get())
        }
    }
}
val repositoryModule = module {

    scope(WEATHER_SCOPE_QUALIFIER) {
        scoped<WeatherRepo> {
            WeatherRepoImpl(get(),get())
        }
    }
}
val constantModules = module {
    scope(WEATHER_SCOPE_QUALIFIER) {
        scoped {
            ResponseHandler()
        }
    }
}

// Gather all app modules
val weatherApp= listOf(
    weatherAppModule,useCaseModule, remoteDataSourceModule, repositoryModule, constantModules
)