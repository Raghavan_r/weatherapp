package com.app.weather.repoImpl

import com.app.weather.networking.Resource
import com.app.weather.networking.ResponseHandler
import com.app.weather.networking.WeatherDataSource
import com.app.weather.repo.WeatherRepo
import com.app.weather.responseModel.CurrentWeatherResponse
import java.lang.Exception

private const val API_KEY_DARKSKY = "20cb3758090abc171ee36ed2ed2f5d68"

class WeatherRepoImpl (private val weatherDataSource: WeatherDataSource,
                       private val responseHandler: ResponseHandler) : WeatherRepo {

    override suspend fun getWeatherData(location : String) : Resource<CurrentWeatherResponse>{
        return try {
            val response = weatherDataSource.getCurrentWeather(API_KEY_DARKSKY,location,"hourly,minutely,flags")
            responseHandler.handleSuccess(response)
        }catch (e : Exception){
            responseHandler.handleException(e)
        }

    }
}