package com.app.weather.viewModel

import android.content.Context
import android.graphics.drawable.Drawable
import android.location.Location
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.app.weather.R
import com.app.weather.di.CoroutinesViewModel
import com.app.weather.listener.LocationUpdateListener
import com.app.weather.responseModel.CurrentWeatherResponse
import com.app.weather.usecase.WeatherHomeUseCase
import com.app.weather.gpstracker.GpsTracker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WeatherHomeViewModel(
    private val context: Context,
    private val weatherHomeUseCase: WeatherHomeUseCase
    ) : CoroutinesViewModel(),LocationUpdateListener{

    private val gpsTracker = GpsTracker(context, this)
    private var location: Location? = null

    private val _validationError = MutableLiveData<String>()
    val validationError: LiveData<String> = _validationError

    private val _response = MutableLiveData<CurrentWeatherResponse>()
    val response: LiveData<CurrentWeatherResponse> = _response

    private val _weatherImg = MutableLiveData<Drawable>()
    val  weatherImg : LiveData<Drawable> = _weatherImg





    override fun updatedLocation(location: Location) {

        viewModelScope.launch(Dispatchers.Main.immediate) {
            try {
                weatherHomeUseCase.getWeatherUpdate(location).apply {
                    this?.let {

                        _response.value = it

                        when {
                            this.currently?.icon.equals("partly-cloudy-day") -> {
                                _weatherImg.value = ContextCompat.getDrawable(context, R.drawable.clouds)
                            }
                            this.currently?.icon.equals("clear-day") -> {
                                _weatherImg.value = ContextCompat.getDrawable(context, R.drawable.clear)
                            }
                            this.currently?.icon.equals("rain") -> {
                                _weatherImg.value = ContextCompat.getDrawable(context, R.drawable.rain)
                            }
                            this.currently?.icon.equals("thunderstorm")->{
                                _weatherImg.value = ContextCompat.getDrawable(context,R.drawable.storm)
                            }
                            else -> {
                                _weatherImg.value = ContextCompat.getDrawable(context, R.drawable.sun)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
                setError(e.localizedMessage)
            }
        }
    }


    fun startLocation() {
        location = gpsTracker.getLoc()
    }

    private fun setError(message: String?) {
        _validationError.value = message
    }

}