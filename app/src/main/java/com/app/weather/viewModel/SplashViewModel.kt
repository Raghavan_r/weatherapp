package com.app.weather.viewModel

import android.content.Context
import android.content.Intent
import com.app.weather.di.CoroutinesViewModel
import com.app.weather.views.WeatherHomeActivity

class SplashViewModel : CoroutinesViewModel(){

    fun getIntent(context: Context) : Intent{
        return Intent(context, WeatherHomeActivity::class.java)
    }
}