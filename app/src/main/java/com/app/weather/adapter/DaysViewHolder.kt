package com.app.weather.adapter

import androidx.recyclerview.widget.RecyclerView
import com.app.weather.databinding.UpcominweeksBinding
import com.app.weather.responseModel.Daily
import com.app.weather.responseModel.Data


class DaysViewHolder(private val binding: UpcominweeksBinding ) :
    RecyclerView.ViewHolder(binding.root) {

    /**
     * binder class to update the UI
     * */
    fun bind(daily: Data) {

        binding.apply {
            binding.tvTempType.text = daily.summary
            binding.tvTemp.text = daily.temperatureHigh.toString() + "\u2103"
        }

    }
}