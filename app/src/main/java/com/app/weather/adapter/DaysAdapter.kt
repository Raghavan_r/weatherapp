package com.app.weather.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.weather.databinding.UpcominweeksBinding
import com.app.weather.responseModel.Daily
import com.app.weather.responseModel.Data


class DaysAdapter :
    RecyclerView.Adapter<DaysViewHolder>() {

    private var daysList: List<Data> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DaysViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = UpcominweeksBinding.inflate(layoutInflater, parent, false)
        return DaysViewHolder(
            itemBinding
        )
    }

    override fun onBindViewHolder(holder: DaysViewHolder, position: Int) {
        holder.bind(daysList[position])
    }

    /**
     * Used to set the data to the adapter
     * @param [items] is the updated student list from the viewModel
     */
    fun setDataList(items: List<Data>) {
        this.daysList = items
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = daysList.size
}
