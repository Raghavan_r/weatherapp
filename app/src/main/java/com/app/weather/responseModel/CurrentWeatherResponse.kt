package com.app.weather.responseModel
import com.google.gson.annotations.SerializedName


data class CurrentWeatherResponse(
    @SerializedName("currently")
    val currently: Currently?,
    @SerializedName("daily")
    val daily: Daily?,
    @SerializedName("latitude")
    val latitude: Double?,
    @SerializedName("longitude")
    val longitude: Double?,
    @SerializedName("offset")
    val offset: Double?,
    @SerializedName("timezone")
    val timezone: String?
)

data class Currently(
    @SerializedName("apparentTemperature")
    val apparentTemperature: Double?,
    @SerializedName("cloudCover")
    val cloudCover: Double?,
    @SerializedName("dewPoint")
    val dewPoint: Double?,
    @SerializedName("humidity")
    val humidity: Double?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("ozone")
    val ozone: Double?,
    @SerializedName("precipIntensity")
    val precipIntensity: Double?,
    @SerializedName("precipProbability")
    val precipProbability: Double?,
    @SerializedName("pressure")
    val pressure: Double?,
    @SerializedName("summary")
    val summary: String?,
    @SerializedName("temperature")
    val temperature: Double?,
    @SerializedName("time")
    val time: Double?,
    @SerializedName("uvIndex")
    val uvIndex: Double?,
    @SerializedName("visibility")
    val visibility: Double?,
    @SerializedName("windBearing")
    val windBearing: Double?,
    @SerializedName("windGust")
    val windGust: Double?,
    @SerializedName("windSpeed")
    val windSpeed: Double?
)

data class Daily(
    @SerializedName("data")
    val data: List<Data>?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("summary")
    val summary: String?
)

data class Data(
    @SerializedName("apparentTemperatureHigh")
    val apparentTemperatureHigh: Double?,
    @SerializedName("apparentTemperatureHighTime")
    val apparentTemperatureHighTime: Double?,
    @SerializedName("apparentTemperatureLow")
    val apparentTemperatureLow: Double?,
    @SerializedName("apparentTemperatureLowTime")
    val apparentTemperatureLowTime: Double?,
    @SerializedName("apparentTemperatureMax")
    val apparentTemperatureMax: Double?,
    @SerializedName("apparentTemperatureMaxTime")
    val apparentTemperatureMaxTime: Double?,
    @SerializedName("apparentTemperatureMin")
    val apparentTemperatureMin: Double?,
    @SerializedName("apparentTemperatureMinTime")
    val apparentTemperatureMinTime: Double?,
    @SerializedName("cloudCover")
    val cloudCover: Double?,
    @SerializedName("dewPoint")
    val dewPoint: Double?,
    @SerializedName("humidity")
    val humidity: Double?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("moonPhase")
    val moonPhase: Double?,
    @SerializedName("ozone")
    val ozone: Double?,
    @SerializedName("precipIntensity")
    val precipIntensity: Double?,
    @SerializedName("precipIntensityMax")
    val precipIntensityMax: Double?,
    @SerializedName("precipIntensityMaxTime")
    val precipIntensityMaxTime: Double?,
    @SerializedName("precipProbability")
    val precipProbability: Double?,
    @SerializedName("precipType")
    val precipType: String?,
    @SerializedName("pressure")
    val pressure: Double?,
    @SerializedName("summary")
    val summary: String?,
    @SerializedName("sunriseTime")
    val sunriseTime: Double?,
    @SerializedName("sunsetTime")
    val sunsetTime: Double?,
    @SerializedName("temperatureHigh")
    val temperatureHigh: Double?,
    @SerializedName("temperatureHighTime")
    val temperatureHighTime: Double?,
    @SerializedName("temperatureLow")
    val temperatureLow: Double?,
    @SerializedName("temperatureLowTime")
    val temperatureLowTime: Double?,
    @SerializedName("temperatureMax")
    val temperatureMax: Double?,
    @SerializedName("temperatureMaxTime")
    val temperatureMaxTime: Double?,
    @SerializedName("temperatureMin")
    val temperatureMin: Double?,
    @SerializedName("temperatureMinTime")
    val temperatureMinTime: Double?,
    @SerializedName("time")
    val time: Double?,
    @SerializedName("uvIndex")
    val uvIndex: Double?,
    @SerializedName("uvIndexTime")
    val uvIndexTime: Double?,
    @SerializedName("visibility")
    val visibility: Double?,
    @SerializedName("windBearing")
    val windBearing: Double?,
    @SerializedName("windGust")
    val windGust: Double?,
    @SerializedName("windGustTime")
    val windGustTime: Double?,
    @SerializedName("windSpeed")
    val windSpeed: Double?
)