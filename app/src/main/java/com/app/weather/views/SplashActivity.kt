package com.app.weather.views

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.app.weather.R
import com.app.weather.databinding.ActivitySplashBinding
import com.app.weather.viewModel.SplashViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

private const val DELAY_TIME = 3000
private const val LOCATION_PERMISSIONS_REQUEST = 1000

class SplashActivity : BaseActivity() {

    private val viewModel: SplashViewModel by viewModelScope?.viewModel(this)
    private lateinit var binding: ActivitySplashBinding

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView<ActivitySplashBinding>(this, R.layout.activity_splash)
        binding.splashViewModel = viewModel

        if (!locationPermissionGranted) {
            requestLocationPermission()
        }else{
            navigate()
        }

    }

    private val locationPermissionGranted
        get() = let {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun navigate() {

        GlobalScope.launch(Dispatchers.Main.immediate) {
            delay(DELAY_TIME.toLong())
            startActivity(viewModel.getIntent(this@SplashActivity))
            finish()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_PERMISSIONS_REQUEST && grantResults.size == 1 &&
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            navigate()
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestLocationPermission() {
        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
            val view: View? = binding.root
            if (view != null) {
                Snackbar.make(
                    view, getString(R.string.permission_error),
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction("Ok") {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            ),
                            LOCATION_PERMISSIONS_REQUEST
                        )
                    }
                    .show()
            }
        } else {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ), LOCATION_PERMISSIONS_REQUEST
            )
            if (!locationPermissionGranted) {
                requestLocationPermission()
            }else{
                navigate()
            }
        }
    }
}
