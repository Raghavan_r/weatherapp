package com.app.weather.views

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.weather.R
import com.app.weather.di.WEATHER_SCOPE_ID
import com.app.weather.di.WEATHER_SCOPE_QUALIFIER
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import org.koin.android.ext.android.getKoin
import org.koin.core.scope.Scope
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext

abstract class BaseActivity (
    private val parentContext: CoroutineContext = EmptyCoroutineContext
): AppCompatActivity(){

    protected lateinit var coroutineScope: CoroutineScope
    private set
    val viewModelScope: Scope? get() = _currentScope
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        coroutineScope =
            CoroutineScope(Dispatchers.Main + parentContext + SupervisorJob(parentContext[Job]))
    }

    override fun onDestroy() {
        coroutineScope.cancel()
        super.onDestroy()
    }

    private fun internalCreateScope(): Scope? {
        val created = getKoin().getScopeOrNull(WEATHER_SCOPE_ID) == null
        val scope = getKoin()
            .getOrCreateScope(WEATHER_SCOPE_ID, WEATHER_SCOPE_QUALIFIER)

        if (created) {
            scope.declare(this)
        }

        return scope
    }

    private var _currentScope = internalCreateScope()

    /**
     * Destroy/close out a logged in scope. Should only be called when a user is logging out
     */
    fun destroyLoggedInScope() {
        _currentScope.also { scope ->
            _currentScope = null
            scope?.close()
        }
    }

    /**
     * Start a new logged in scope.  If one is already active then it is returned without creation
     */
    fun startLoggedInScope(): Scope? = internalCreateScope().apply {
        _currentScope = this
    }

    protected fun showErrorMessage(message: String, view: View) {
        val snack = Snackbar.make(
            view,
            message,
            Snackbar.LENGTH_INDEFINITE
        )
        snack.setAction(resources.getString(R.string.ok)) {
            snack.dismiss()
        }
        snack.show()
    }

}

